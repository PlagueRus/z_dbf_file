DBF PL/SQL library
==============================

Descritpion
-----------
This package allows to generate simple DBF files. It's written in pure 
PL/SQL and does not require access to external Java functions.

Author
------
This library is written by Dmitry Leushin Dmitry.Leushin@gmail.com.

License
-------
The MIT License (MIT)

Copyright (c) 2014 Dmitry Leushin

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the Software), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, andor sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

Limitations
-----------
Currently, due to some restrictions, generated data cannot be returned as CLOB for
multibyte characters sets.

Currenctly supports only NUMBER, CHAR, DATE data types of DBF file fields.

Examples
--------

begin  
  
  Z_DBF_FILE.init;
  
  Z_DBF_FILE.AddField('NUMFIELD', 'N', 15, 2);  
  Z_DBF_FILE.AddField('STRFIELD', 'C', 10);  
  Z_DBF_FILE.AddField('DATFIELD', 'D');  
  
  -- loop 

  Z_DBF_FILE.SetFieldValueNumber('NUMFIELD', 1);  
  Z_DBF_FILE.SetFieldValueString('STRFIELD', 'TEST');  
  Z_DBF_FILE.SetFieldValueDate('DATFIELD', sysdate);  
    
  Z_DBF_FILE.OutputRow;  

  -- end loop
  
  :result := Z_DBF_FILE.getBlob;  
end; 

results in :result blob variable