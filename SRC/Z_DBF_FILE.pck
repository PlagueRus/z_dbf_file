create or replace package Z_DBF_FILE is

-- The MIT License (MIT)
-- Copyright (c) 2014 Dmitry Leushin
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the Software), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, andor sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-- THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  -- Пакет Формирования DBF файла
  -- %author  Дмитрий Леушин
  -- %version 1.1
  -- %usage {*} 1. {%link Z_DBF_FILE.Init Init}
  --        {*} 2. {%link Z_DBF_FILE.AddField AddField}
  --        {*} 3. {%link Z_DBF_FILE.SetFieldValueString SetFieldValue*****}
  --        {*} 4. {%link Z_DBF_FILE.OutputRow OutputRow}
  --        {*} 5. {%link Z_DBF_FILE.getClob getClob}

  typeNumber constant varchar2(1) := 'N';
  typeString constant varchar2(1) := 'C';
  typeDate   constant varchar2(1) := 'D';
  typeMemo   constant varchar2(1) := 'M';
  
  type tFIELD is record(
    name    varchar2(10),
    ftype   varchar(1),
    offset  number,
    length  number,
    decimal number,
    flags   number);

  type tFields is table of tFIELD index by binary_integer;
  type sFields is table of tFIELD index by varchar2(10);

  type tDBF is record(
    dbftype varchar2(64),
    dbfdate date,
    
    current_row number,
    dbffile     blob,
    fieldsByNum tFields,
    fieldsByStr sFields,
    
    offset  number,
    rowsize number,
    
    hasMemo  number(1),
    hasIndex number(1),
    codepage varchar2(32),
    
    column_count number,
    row_count    number,
    
    fptFile    blob,
    block_size number);

  ------------------------------------------------------------
  -- Создание DBF
  ------------------------------------------------------------

  -- Инициализация перед началом формирования нового файла
  -- %param enc Наименование кодировки RU8PC866 или CL8MSWIN1251
  procedure Init(enc varchar2 := 'RU8PC866');

  -- Добавляет колонку в структуру DBF заданного типа
  -- %param name Имя колонки
  -- %param type Тип колонки ("C" - строка, "N" - число, "D" - дата)
  -- %param length Длина (для Даты всегда равно 8)
  -- %param decimal Количество знаков после запятой (для Строки и Даты всегда равно 0)
  procedure AddField(name    varchar2,
                     type    varchar2,
                     length  number := 8,
                     decimal number := 0);

  -- Установливает значение выбранного поля новой записи типа Строка
  -- %param name Имя колонки
  -- %param value Новое значение
  procedure SetFieldValueString(name varchar2, value varchar2);

  -- Установливает значение выбранного поля новой записи типа Число
  -- %param name Имя колонки
  -- %param value Новое значение
  procedure SetFieldValueNumber(name varchar2, value number);

  -- Установливает значение выбранного поля новой записи типа Дата
  -- %param name Имя колонки
  -- %param value Новое значение
  procedure SetFieldValueDate(name varchar2, value date);

  -- Формирует заполненную запись в DBF файл
  procedure OutputRow;

  -- Формирует DBF файл в виде CLOB данных
  -- %return Возвращает содержимое сформированного DBF файла в виде CLOB данных
  function getClob return clob;
  function getBlob return blob;

  ------------------------------------------------------------
  -- Чтение DBF
  ------------------------------------------------------------

  -- Читает DBF+FPT файл, возвращает "курсор"
  -- %param datafile Содержимое DBF-файла
  -- %param memofile Содержимое FPT-файла
  -- %param dbfcursor курсор на загруженые данные
  procedure ReadBlob(tdatafile in blob,
                     memofile  in blob := null,
                     dbfcursor out NOCOPY tDBF);

  -- Проверяет прочитаны ли все данные из курсора
  -- %param dbffile структура tDBF определяющая DBF-файл
  function isEOF(dbffile in out NOCOPY tDBF) return number;

  -- Переходит к следующей строке
  -- %param dbffile структура tDBF определяющая DBF-файл
  function NextRow(dbffile in out NOCOPY tDBF) return number;

  -- Получает тип поля DBF
  -- %param dbffile структура tDBF определяющая DBF-файл
  -- %param name наименование поля, прописными для ВИА-файлов
  function GetFieldType(dbffile in out NOCOPY tDBF, name varchar2)
    return varchar2;

  -- Получает наименование поля DBF по индексу
  -- %param dbffile структура tDBF определяющая DBF-файл
  -- %param indx индекс поля 1..*
  function GetFieldName(dbffile in out NOCOPY tDBF, indx number)
    return varchar2;

  -- Получает количество полей DBF
  -- %param dbffile структура tDBF определяющая DBF-файл
  function GetFieldsCount(dbffile in out NOCOPY tDBF) return number;

  -- %param dbffile структура tDBF определяющая DBF-файл
  -- %param name наименование поля, прописными для ВИА-файлов
  function GetFieldValueNumber(dbffile in out NOCOPY tDBF, name varchar2)
    return number;
  -- %param dbffile структура tDBF определяющая DBF-файл
  -- %param name наименование поля, прописными для ВИА-файлов
  function GetFieldValueString(dbffile in out NOCOPY tDBF, name varchar2)
    return varchar2;
  -- %param dbffile структура tDBF определяющая DBF-файл
  -- %param name наименование поля, прописными для ВИА-файлов
  function GetFieldValueDate(dbffile in out NOCOPY tDBF, name varchar2)
    return date;

  --function GetFieldValueMemo(dbffile tDBF, name varchar2) return CLOB;

  -- Сбрасывает состояние чтения BLOB, т.е. будем читать заново
  -- %param dbffile структура tDBF определяющая DBF-файл
  procedure Reset(dbffile in out NOCOPY tDBF);

  -- Высвобождает ресурсы (необязательно)
  -- %param dbffile структура tDBF определяющая DBF-файл
  procedure Release(dbffile in out NOCOPY tDBF);
end Z_DBF_FILE;
/
create or replace package body Z_DBF_FILE is
  type DBF_HEADER is record(
    hdr_version   varchar2(2), -- 1*2, ==3
    hdr_year      varchar2(2), -- 1*2
    hdr_month     varchar2(2), -- 1*2
    hdr_day       varchar2(2), -- 1*2
    hdr_recno     number(9), -- 4
    hdr_headerlen number(5), -- 2
    hdr_reclen    number(5), -- 2
    hdr_reserved1 varchar2(34), -- 17*2
    hdr_language  varchar2(2), -- 1*2
    hdr_reserved2 varchar2(4) -- 2*2
    );
  type DBF_FIELD is record(
    fld_name      varchar2(22), -- 10 + 1 !! \
    fld_type      varchar2(2), -- 1          |
    fld_offset    number(9), -- 4            |
    fld_length    number(3), -- 1            >= 32
    fld_decimal   number(3), -- 1            |
    fld_reserved1 varchar2(28), -- 14        /
    
    fld_num_tmp number(20, 10),
    fld_str_tmp varchar2(510),
    fld_dat_tmp date);
  type DBF_FIELD_LIST is table of DBF_FIELD index by binary_integer;

  dbfheader DBF_HEADER;
  dbffields DBF_FIELD_LIST;
  
  to_encoding   varchar2(32) := 'RU8PC866'; -- 
  curr_encoding varchar2(32) := nls_charset_name(nls_charset_id('CHAR_CS')); -- 'AL32UTF8';

  begin_of_record_delete_flag number(17) := 32; -- 0x20
  end_of_header_flag          varchar2(2); -- 0D
  end_of_data_flag            varchar2(2); -- 1A

  rowdata       blob;

  function N2H(N number) return varchar2 as
  begin
    return rawtohex(utl_raw.cast_to_raw(CHR(N)));
  end;

  function H2B(S varchar2) return blob as
  begin
    return to_blob((hextoraw(S)));
  end;

  function S2H(S varchar2, n number, delim varchar2 := ' ') return varchar2 as
  begin
    return rawtohex(utl_raw.cast_to_raw(CONVERT(S,
                                                to_encoding,
                                                curr_encoding))) || rawtohex(utl_raw.cast_to_raw(CONVERT(RPAD(delim,
                                                                                                              n -
                                                                                                              nvl(length(S),
                                                                                                                  0),
                                                                                                              delim),
                                                                                                         to_encoding,
                                                                                                         curr_encoding)));
  end;

  procedure Init(enc varchar2) is
  begin
    to_encoding := Init.enc;
  
    end_of_header_flag := N2H(13); -- 0D
    end_of_data_flag   := N2H(26); -- 1A
  
    dbfheader.hdr_version   := N2H(3);
    dbfheader.hdr_year      := N2H(TO_NUMBER(TO_CHAR(sysdate, 'YY')));
    dbfheader.hdr_month     := N2H(TO_NUMBER(TO_CHAR(sysdate, 'MM')));
    dbfheader.hdr_day       := N2H(TO_NUMBER(TO_CHAR(sysdate, 'DD')));
    dbfheader.hdr_recno     := 0;
    dbfheader.hdr_headerlen := 32 + 1;
    dbfheader.hdr_reclen    := 1;
    dbfheader.hdr_reserved1 := N2H(0) || N2H(0) || N2H(0) || N2H(0) ||
                               N2H(0) || N2H(0) || N2H(0) || N2H(0) ||
                               N2H(0) || N2H(0) || N2H(0) || N2H(0) ||
                               N2H(0) || N2H(0) || N2H(0) || N2H(0) ||
                               N2H(0);
    dbfheader.hdr_language  := CASE WHEN to_encoding = 'RU8PC866' THEN N2H(101) 
                               WHEN to_encoding = 'CL8MSWIN1251' THEN N2H(201) 
                               ELSE N2H(0) END;
                                -- 0x65
    dbfheader.hdr_reserved2 := N2H(0) || N2H(0);
  
    --rowdata := null;
    dbms_lob.createtemporary(lob_loc => rowdata, cache => true);
    --      dbms_output.put_line(dbms_lob.getlength(TO_CLOB(dbfheader.hdr_reserved1)));
    dbffields.delete;
  end;

  procedure AddField(name    varchar2,
                     type    varchar2,
                     length  number := 8,
                     decimal number := 0) is
    newoffset number(9);
    newid     number(3);
  begin
    newoffset := 1;
    newid     := dbffields.count + 1;
    for i in 1 .. dbffields.count loop
      newoffset := newoffset + dbffields(i).fld_length;
      --dbms_output.put_line(i || ',' || newoffset);
    end loop;
    dbffields(newid).fld_name := UPPER(name);
    dbffields(newid).fld_type := UPPER(type);
    dbffields(newid).fld_offset := newoffset;
    dbffields(newid).fld_length := length;
    dbffields(newid).fld_decimal := decimal;
    dbffields(newid).fld_reserved1 := N2H(0) || N2H(0) || N2H(0) || N2H(0) ||
                                      N2H(0) || N2H(0) || N2H(0) || N2H(0) ||
                                      N2H(0) || N2H(0) || N2H(0) || N2H(0) ||
                                      N2H(0) || N2H(0);
  
    dbffields(newid).fld_num_tmp := null;
    dbffields(newid).fld_str_tmp := null;
    dbffields(newid).fld_dat_tmp := null;
  
    dbfheader.hdr_headerlen := dbfheader.hdr_headerlen + 32;
    dbfheader.hdr_reclen    := dbfheader.hdr_reclen + length;
    --dbms_output.put_line(newoffset);
    --!! временные переменные в
  end;

  procedure SetFieldValueString(name varchar2, value varchar2) is
  begin
    for i in 1 .. dbffields.count loop
      if dbffields(i).fld_name = UPPER(name) then --and dbffields(i).fld_type = 'C' then
        dbffields(i).fld_str_tmp := SUBSTR(value,
                                           1,
                                           dbffields(i).fld_length);
      end if;
    end loop;
  end;

  procedure SetFieldValueNumber(name varchar2, value number) is
  begin
    for i in 1 .. dbffields.count loop
      if dbffields(i).fld_name = UPPER(name) then --and dbffields(i).fld_type = 'N' then
        dbffields(i).fld_num_tmp := value;
      end if;
    end loop;
  end;

  procedure SetFieldValueDate(name varchar2, value date) is
  begin
    for i in 1 .. dbffields.count loop
      if dbffields(i).fld_name = UPPER(name) then --and dbffields(i).fld_type = 'D' then
        dbffields(i).fld_dat_tmp := value;
      end if;
    end loop;
  end;

  procedure OutputRow is
    tmprow blob;
    format varchar2(32);
    tstr   varchar2(250);
  begin
    dbms_lob.createtemporary(tmprow, true);
    DBMS_LOB.append(tmprow, H2B('20'));

    for i in 1 .. dbffields.count loop
    
      if dbffields(i).fld_type = 'C' then
        
        DBMS_LOB.append(tmprow,
                        H2B(S2H(dbffields(i).fld_str_tmp,
                                dbffields(i).fld_length)));
      end if;
    
      if dbffields(i).fld_type = 'N' then
        if dbffields(i).fld_decimal > 0 then
          format := LPAD(RPAD('.', dbffields(i).fld_decimal, '9'),
                         dbffields(i).fld_length,
                         '9');
        else
          format := LPAD('9', dbffields(i).fld_length, '9');
        end if;
        tstr := SUBSTR(LPAD(trim(TO_CHAR(dbffields(i).fld_num_tmp, format)),
                            dbffields(i).fld_length,
                            ' '),
                       1,
                       dbffields(i).fld_length);
        DBMS_LOB.append(tmprow,
                        H2B(S2H(tstr, dbffields(i).fld_length)));
      end if;
    
      if dbffields(i).fld_type = 'D' then
        if dbffields(i).fld_dat_tmp is not null then
          DBMS_LOB.append(tmprow,
                          H2B(S2H(TO_CHAR(dbffields(i).fld_dat_tmp,
                                          'YYYYMMDD'),
                                  dbffields(i).fld_length)));
        else
          DBMS_LOB.append(tmprow,
                          H2B(S2H('        ', dbffields(i).fld_length)));
        end if;
      end if;
    
    end loop;
  
    dbms_lob.append(rowdata, tmprow);
    dbfheader.hdr_recno := dbfheader.hdr_recno + 1;
  
    for i in 1 .. dbffields.count loop
      dbffields(i).fld_str_tmp := null;
      dbffields(i).fld_num_tmp := null;
      dbffields(i).fld_dat_tmp := null;
    end loop;
  end;

  function NumberToChar(value number) return varchar2 is
  begin
    return N2H(mod(value, 256));
  end;

  function NumberToShort(value number) return varchar2 is
  begin
    -- SUBSTR(rawtohex(UTL_RAW.cast_from_binary_integer(250*16,2)),1,4)
    return N2H(mod(value, 256)) || N2H(mod(floor(value / 256), 256));
  end;

  function NumberToInt(value number) return varchar2 is
  begin
    -- rawtohex(UTL_RAW.cast_from_binary_integer(250*16,2))
    return N2H(mod(value, 256)) || N2H(mod(floor(value / 256), 256)) || N2H(mod(floor(value /
                                                                                      (256 * 256)),
                                                                                256)) || N2H(mod(floor(value /
                                                                                                       (256 * 256 * 256)),
                                                                                                 256));
  end;

  function getHeader return blob is
    blobtmp blob;
    --    hdrtmp  varchar2(4000);
    --    strtmp  varchar2(128);
  begin
    dbms_lob.createtemporary(blobtmp, true, dbms_lob.call);
    DBMS_LOB.append(blobtmp, H2B(dbfheader.hdr_version));
    --hdrtmp := hdrtmp || dbfheader.hdr_version;
    DBMS_LOB.append(blobtmp, H2B(dbfheader.hdr_year));
    --hdrtmp := hdrtmp || dbfheader.hdr_year;
    DBMS_LOB.append(blobtmp, H2B(dbfheader.hdr_month));
    --    hdrtmp := hdrtmp || dbfheader.hdr_month;
    DBMS_LOB.append(blobtmp, H2B(dbfheader.hdr_day));
    --    hdrtmp := hdrtmp || dbfheader.hdr_day;
    DBMS_LOB.append(blobtmp, H2B(NumberToInt(dbfheader.hdr_recno)));
    --    hdrtmp := hdrtmp || NumberToInt(dbfheader.hdr_recno);
    DBMS_LOB.append(blobtmp, H2B(NumberToShort(dbfheader.hdr_headerlen)));
    --    hdrtmp := hdrtmp || NumberToShort(dbfheader.hdr_headerlen);
    DBMS_LOB.append(blobtmp, H2B(NumberToShort(dbfheader.hdr_reclen)));
    --    hdrtmp := hdrtmp || NumberToShort(dbfheader.hdr_reclen);
    DBMS_LOB.append(blobtmp, H2B(dbfheader.hdr_reserved1));
    --    hdrtmp := hdrtmp || dbfheader.hdr_reserved1;
    DBMS_LOB.append(blobtmp, H2B(dbfheader.hdr_language));
    --    hdrtmp := hdrtmp || dbfheader.hdr_language;
    DBMS_LOB.append(blobtmp, H2B(dbfheader.hdr_reserved2));
    --    hdrtmp := hdrtmp || dbfheader.hdr_reserved2;
    for i in 1 .. dbffields.count loop
    
      --      strtmp := SUBSTR(dbffields(i).fld_name, 1, 10);
      DBMS_LOB.append(blobtmp, H2B(S2H(dbffields(i).fld_name, 11, CHR(0))));
      --      for k in 1 .. (11 - length(SUBSTR(dbffields(i).fld_name, 1, 10))) loop
      --        strtmp := strtmp || N2H(0);
      --      end loop;
      --      dbms_output.put_line(length(strtmp));
      DBMS_LOB.append(blobtmp, H2B(S2H(dbffields(i).fld_type, 1)));
      --      strtmp := strtmp || SUBSTR(dbffields(i).fld_type, 1, 1);
      --      dbms_output.put_line(length(strtmp));
      DBMS_LOB.append(blobtmp, H2B(NumberToInt(dbffields(i).fld_offset)));
      --      strtmp := strtmp || NumberToInt(dbffields(i).fld_offset);
      --      dbms_output.put_line(length(strtmp));
      DBMS_LOB.append(blobtmp, H2B(NumberToChar(dbffields(i).fld_length)));
      --      strtmp := strtmp || NumberToChar(dbffields(i).fld_length);
      --      dbms_output.put_line(length(strtmp));
      DBMS_LOB.append(blobtmp, H2B(NumberToChar(dbffields(i).fld_decimal)));
      --      strtmp := strtmp || NumberToChar(dbffields(i).fld_decimal);
      --      dbms_output.put_line(length(strtmp));
      DBMS_LOB.append(blobtmp, H2B(dbffields(i).fld_reserved1));
      --      strtmp := strtmp || dbffields(i).fld_reserved1;
    --      dbms_output.put_line('total:' || to_char(length(strtmp)));
    
    --      DBMS_LOB.append(hdrtmp, blobtmp);
    --      hdrtmp := hdrtmp || strtmp;
    end loop;
    DBMS_LOB.append(blobtmp, H2B(end_of_header_flag));
    --    dbms_lob.writeappend(blobtmp, length(hdrtmp), hdrtmp);
    return blobtmp;
  end;

  function getBlob return blob is
    blobtmp blob;
  begin
    dbms_lob.createtemporary(blobtmp, false);
    dbms_lob.append(blobtmp, getHeader);
    dbms_lob.append(blobtmp, rowdata);
    dbms_lob.append(blobtmp, H2B(end_of_data_flag));
    return blobtmp;
  end;

  function getClob return clob is
    blobtmp blob;
    clobtmp clob;
    
    dest_offsset integer := 1;
    src_offsset  integer := 1;
    lang_context integer := dbms_lob.default_lang_ctx;
    warning      integer;    
  begin
    dbms_lob.createtemporary(blobtmp, false);
    dbms_lob.append(blobtmp, getHeader);
    dbms_lob.append(blobtmp, rowdata);
    dbms_lob.append(blobtmp, H2B(end_of_data_flag));
    
    dbms_lob.createTemporary(clobtmp, false);
    dbms_lob.converttoclob(dest_lob     => clobtmp
                           ,src_blob     => blobtmp
                           ,amount       => dbms_lob.lobmaxsize
                           ,dest_offset  => dest_offsset
                           ,src_offset   => src_offsset
                           ,blob_csid    => dbms_lob.default_csid
                           ,lang_context => lang_context
                           ,warning      => warning);
    
    return clobtmp;
  end;

  function getDWORDNumber(dbfdata blob, offset number) return number is
    dword varchar2(4);
    ret   number(10);
  begin
    dword := utl_raw.cast_to_varchar2(dbms_lob.substr(lob_loc => dbfdata,
                                                      amount  => 4,
                                                      offset  => offset));
    ret   := coalesce(ascii(substr(dword, 1, 1)), 0) +
             coalesce(ascii(substr(dword, 2, 1)), 0) * 256 +
             coalesce(ascii(substr(dword, 3, 1)), 0) * 256 * 256 +
             coalesce(ascii(substr(dword, 4, 1)), 0) * 256 * 256 * 256;
    return ret;
  end;

  function getWORDNumber(dbfdata blob, offset number) return number is
    word varchar2(4);
    ret  number(10);
  begin
    word := utl_raw.cast_to_varchar2(dbms_lob.substr(lob_loc => dbfdata,
                                                     amount  => 2,
                                                     offset  => offset));
    ret  := coalesce(ascii(substr(word, 1, 1)), 0) +
            coalesce(ascii(substr(word, 2, 1)), 0) * 256;
    return ret;
  end;

  function getByteNumber(dbfdata blob, offset number) return number is
    word varchar2(4);
    ret  number(10);
  begin
    word := utl_raw.cast_to_varchar2(dbms_lob.substr(lob_loc => dbfdata,
                                                     amount  => 1,
                                                     offset  => offset));
    ret  := coalesce(ascii(substr(word, 1, 1)), 0);
    return ret;
  end;

  function getDateDate(dbfdata blob, offset number) return date is
    word varchar2(4);
    ret  date;
  begin
    word := utl_raw.cast_to_varchar2(dbms_lob.substr(lob_loc => dbfdata,
                                                     amount  => 3,
                                                     offset  => offset));
    if coalesce(ascii(substr(word, 1, 1)), 0) > 100 then
      ret := to_date(to_char(coalesce(ascii(substr(word, 1, 1)), 0) + 1900,
                             '0000') ||
                     to_char(coalesce(ascii(substr(word, 2, 1)), 0), '00') ||
                     to_char(coalesce(ascii(substr(word, 3, 1)), 0), '00'),
                     'yyyymmdd');
    else
      ret := to_date(to_char(coalesce(ascii(substr(word, 1, 1)), 0), '00') ||
                     to_char(coalesce(ascii(substr(word, 2, 1)), 0), '00') ||
                     to_char(coalesce(ascii(substr(word, 3, 1)), 0), '00'),
                     'yymmdd');
    end if;
    return ret;
  end;

  function getCharString(dbfdata blob, offset number, ssize number := 1)
    return varchar2 is
    line varchar2(256);
  begin
    line := utl_raw.cast_to_varchar2(dbms_lob.substr(lob_loc => dbfdata,
                                                     amount  => ssize,
                                                     offset  => offset));
    return rtrim(line, N2H(0));
  end;

  function stringDecode(dbffile in tDBF, str in varchar2) return varchar2 is
  begin
    if dbffile.codepage <> curr_encoding then			
      return rtrim(convert(str, curr_encoding, dbffile.codepage));
    else
      return rtrim(str);
    end if;
  end;

  procedure ReadBlob(tdatafile in blob,
                     memofile  in blob := null,
                     dbfcursor out NOCOPY tDBF) is
    idx number;
    fieldoffset constant number := 33;
    fieldsize   constant number := 32;
    runoffset number(17) := 0;
    datafile  blob;
  begin
    dbms_lob.createtemporary(datafile, true, dbms_lob.call);
    dbms_lob.copy(datafile, tdatafile, dbms_lob.getlength(tdatafile));
    dbfcursor.dbffile := datafile;
    dbfcursor.fptFile := memofile;
    dbfcursor.dbftype := case getByteNumber(datafile, 1)
                           when 2 then
                            'FoxBASE'
                           when 3 then
                            'FoxBASE+/Dbase III plus, no memo'
                           when 48 then
                            'Visual FoxPro'
                           when 49 then
                            'Visual FoxPro, autoincrement enabled'
                           when 50 then
                            'Visual FoxPro with field type Varchar or Varbinary'
                           when 67 then
                            'dBASE IV SQL table files, no memo'
                           when 99 then
                            'dBASE IV SQL system files, no memo'
                           when 131 then
                            'FoxBASE+/dBASE III PLUS, with memo'
                           when 139 then
                            'dBASE IV with memo'
                           when 203 then
                            'dBASE IV SQL table files, with memo'
                           when 229 then
                            'HiPer-Six format with SMT memo file'
                           when 245 then
                            'FoxPro 2.x (or earlier) with memo'
                           when 251 then
                            'FoxBASE'
                           else
                            'unknown'
                         end;
  
    dbfcursor.dbfdate   := getDateDate(datafile, 2);
    dbfcursor.row_count := getDWORDNumber(datafile, 5);
    dbfcursor.offset    := getWORDNumber(datafile, 9);
    dbfcursor.rowsize   := getWORDNumber(datafile, 11);
  
    dbfcursor.hasMemo  := 0;
    dbfcursor.hasIndex := 0;
  
    case getByteNumber(datafile, 29)
      when 1 then
        dbfcursor.hasIndex := 1;
      when 2 then
        dbfcursor.hasMemo := 1;
      when 3 then
        dbfcursor.hasIndex := 1;
        dbfcursor.hasMemo  := 1;
      when 0 then
        null;
      else
        raise_application_error(-20000,
                                'Неверный тип DBF-файла');
        --P_EXCEPTION(0, 'Неверный тип DBF-файла');
    end case;
  
    if dbfcursor.hasMemo = 0 and memofile is not null then
      raise_application_error(-20000,
                              'DBF файл не содержит memo-полей, но передан FPT-файл');
      --P_EXCEPTION(0, 'DBF файл не содержит memo-полей, но передан FPT-файл');
    end if;
  
    case getByteNumber(datafile, 30)
      when 201 then
        dbfcursor.codepage := 'CL8MSWIN1251';
      when 101 then
        dbfcursor.codepage := 'RU8PC866';
	  when 0 then
		dbfcursor.codepage := 'RU8PC866';
	  else
		raise_application_error(-20000, 'Байт кодовой страницы ' || getByteNumber(datafile, 30) || ' не предусмотрен в данном пакете. Требуется доработка пакета');
    end case;
  
    /*                dbms_output.put_line('DBF type: ' || dbfcursor.dbftype);
    dbms_output.put_line('DBF date: ' || dbfcursor.dbfdate);
    dbms_output.put_line('Row count: ' || dbfcursor.row_count);
    dbms_output.put_line('Rows offset: ' || dbfcursor.offset);
    dbms_output.put_line('Row size: ' || dbfcursor.rowsize);
    dbms_output.put_line('Has Memo: ' || case when dbfcursor.hasMemo = 1 then 'true' else
                                             'false' end);
    dbms_output.put_line('Has Index: ' || case when dbfcursor.hasIndex = 1 then 'true' else
                                             'false' end);*/
  
    idx                    := 0;
    dbfcursor.column_count := 0;
  
    while getByteNumber(datafile, fieldoffset + idx * fieldsize) <> 13 loop
      if idx > 254 then
        raise_application_error(-20000,
                                'Не верный формат файла DBF (блок описания полей)');
        --P_EXCEPTION(0, 'Не верный формат файла DBF (блок описания полей)');
      end if;
    
      dbfcursor.fieldsByNum(idx + 1).name := getCharString(datafile,
                                                           fieldoffset +
                                                           idx * fieldsize,
                                                           10);
      dbfcursor.fieldsByNum(idx + 1).ftype := getCharString(datafile,
                                                            fieldoffset +
                                                            idx * fieldsize + 11,
                                                            1);
      dbfcursor.fieldsByNum(idx + 1).offset := getDWORDNumber(datafile,
                                                              fieldoffset +
                                                              idx *
                                                              fieldsize + 12);
      dbfcursor.fieldsByNum(idx + 1).length := getByteNumber(datafile,
                                                             fieldoffset +
                                                             idx * fieldsize + 16);
      dbfcursor.fieldsByNum(idx + 1).decimal := getByteNumber(datafile,
                                                              fieldoffset +
                                                              idx *
                                                              fieldsize + 17);
      dbfcursor.fieldsByNum(idx + 1).flags := getByteNumber(datafile,
                                                            fieldoffset +
                                                            idx * fieldsize + 18);
    
      if dbfcursor.fieldsByNum(idx + 1).offset = 0 then
        dbfcursor.fieldsByNum(idx + 1).offset := runoffset + 1; -- не забываем о символе удаления
      end if;
    
      runoffset := runoffset + dbfcursor.fieldsByNum(idx + 1).length;
    
      dbfcursor.fieldsByStr(dbfcursor.fieldsByNum(idx + 1).name) := dbfcursor.fieldsByNum(idx + 1);
      idx := idx + 1;
      dbfcursor.column_count := dbfcursor.column_count + 1;
    
    /*                        dbms_output.put_line(' Field name: ' || dbfcursor.fieldsByNum(idx).name);
                                                    dbms_output.put_line(' Field type: ' || dbfcursor.fieldsByNum(idx).ftype ||
                                                                                             ' Displacement: ' || dbfcursor.fieldsByNum(idx).offset ||
                                                                                             ' Length: ' || dbfcursor.fieldsByNum(idx).length || ' Decimal: ' || dbfcursor.fieldsByNum(idx)
                                                                                             .decimal || ' Flags: ' || dbfcursor.fieldsByNum(idx).flags);*/
    end loop;
  
    dbfcursor.current_row := 0;
  end;

  function isEOF(dbffile in out NOCOPY tDBF) return number is
    ret number(1) := 1;
  begin
    if (dbffile.dbffile is not null) then
      if dbffile.current_row <= dbffile.row_count then
        ret := 0;
      end if;
    end if;
    return ret;
  end;

  function NextRow(dbffile in out NOCOPY tDBF) return number is
    ret number(17) := 1;
  begin
    if (isEOF(dbffile) = 0) then
      dbffile.current_row := dbffile.current_row + 1;
      if isEOF(dbffile) = 1 then
        ret := 0;
      else
        while getByteNumber(dbffile.dbffile,
                            dbffile.offset + 1 +
                            (dbffile.current_row - 1) * dbffile.rowsize) <>
              begin_of_record_delete_flag and ret = 1 loop
          dbffile.current_row := dbffile.current_row + 1;
          if isEOF(dbffile) = 1 then
            ret := 0;
            continue;
          end if;
        end loop;
      end if;
      if ret = 1 then
        ret := dbffile.current_row;
      end if;
    end if;
    return ret;
  end;

  function GetFieldType(dbffile in out NOCOPY tDBF, name varchar2)
    return varchar2 is
    ret varchar2(10);
  begin
    if (dbffile.dbfdate is not null and
       dbffile.fieldsByStr.EXISTS(name) = true) then
      ret := dbffile.fieldsByStr(name).ftype;
      if (ret = 'C') then
        ret := ret || dbffile.fieldsByStr(name).length;
      end if;
      if (ret = 'N') then
        ret := ret || dbffile.fieldsByStr(name).length || '.' || dbffile.fieldsByStr(name)
              .decimal;
      end if;
    end if;
    return ret;
  end;

  function GetFieldName(dbffile in out NOCOPY tDBF, indx number)
    return varchar2 is
    ret varchar2(10);
  begin
    if (dbffile.dbfdate is not null and
       dbffile.fieldsByNum.EXISTS(indx) = true) then
      ret := dbffile.fieldsByNum(indx).name;
    end if;
    return ret;
  end;

  function GetFieldsCount(dbffile in out NOCOPY tDBF) return number is
  begin
    return dbffile.fieldsByStr.Count;
  end;

  function GetFieldValueNumber(dbffile in out NOCOPY tDBF, name varchar2)
    return number is
    ret  number(30, 10);
    rets varchar2(256);
  begin
    if dbffile.dbffile is not null and isEOF(dbffile) = 0 then
      --if dbffile.fieldsByStr(name).ftype = 'N' then
        rets := ltrim(getCharString(dbffile.dbffile,
                                    dbffile.offset + (dbffile.current_row - 1) *
                                     dbffile.rowsize + dbffile.fieldsByStr(name)
                                    .offset + 1,
                                    dbffile.fieldsByStr(name).length));
        ret  := to_number(rets);
      --end if;
    end if;
    return ret;
  end;

  function GetFieldValueString(dbffile in out NOCOPY tDBF, name varchar2)
    return varchar2 is
    ret varchar2(254);
  begin
    if dbffile.dbffile is not null and isEOF(dbffile) = 0 then
      --if dbffile.fieldsByStr(name).ftype = 'C' then
        ret := stringDecode(dbffile,
                            getCharString(dbffile.dbffile,
                                          dbffile.offset + (dbffile.current_row - 1) *
                                           dbffile.rowsize + dbffile.fieldsByStr(name)
                                          .offset + 1,
                                          dbffile.fieldsByStr(name).length));
      --end if;
    end if;
    return ret;
  end;

  function GetFieldValueDate(dbffile in out NOCOPY tDBF, name varchar2)
    return date is
    ret  date;
    rets varchar2(16);
  begin
    if dbffile.dbffile is not null and isEOF(dbffile) = 0 then
      --if dbffile.fieldsByStr(name).ftype = 'D' then
        rets := ltrim(getCharString(dbffile.dbffile,
                                    dbffile.offset + (dbffile.current_row - 1) *
                                     dbffile.rowsize + dbffile.fieldsByStr(name)
                                    .offset + 1,
                                    dbffile.fieldsByStr(name).length));
      
        if trim(rets) is not null then
          ret := to_date(rets, 'yyyymmdd');
        end if;
      --end if;
    end if;
    return ret;
  end;

  --function GetFieldValueMemo(dbffile tDBF, name varchar2) return CLOB;
  procedure Reset(dbffile in out tDBF) is
  begin
    if dbffile.row_count > 0 then
      dbffile.current_row := 0;
    end if;
  end;

  procedure Release(dbffile in out NOCOPY tDBF) is
    tmp blob;
  begin
    if dbffile.dbffile is not null then
      tmp := dbffile.dbffile;
      dbms_lob.FREETEMPORARY(tmp);
    end if;
  end;
begin
  null;
end Z_DBF_FILE;
/
