begin  
  Z_DBF_FILE.init('CL8MSWIN1251'); -- По-умолчанию DOS-866 ('RU8PC866')  
  
  for k in (select rownum rn from all_objects where rownum <= 107) loop  
    Z_DBF_FILE.AddField('N' || trim(to_char(k.rn, '009')), 'N', 15, 2);  
  end loop;  
    
  Z_DBF_FILE.AddField('S108', 'C', 10);  
  Z_DBF_FILE.AddField('S109', 'C', 20);  
  Z_DBF_FILE.AddField('S110', 'C', 30);  
  Z_DBF_FILE.AddField('D111', 'D');  
  
  for s in (select rownum rn from all_objects where rownum <= 1000) loop  
    for k in (select rownum rn from all_objects where rownum <= 107) loop  
      
      Z_DBF_FILE.SetFieldValueNumber('N' || trim(to_char(k.rn, '009')),  
                                     s.rn * 1000 + k.rn);  
    end loop;  
    
    Z_DBF_FILE.SetFieldValueString('S108', 'ТЕСТ' || trim(to_char(s.rn, '009')));  
    Z_DBF_FILE.SetFieldValueString('S109', 'ТЕСТ' || trim(to_char(s.rn, '009')));  
    Z_DBF_FILE.SetFieldValueString('S110', 'ТЕСТ' || trim(to_char(s.rn, '009')));  
    Z_DBF_FILE.SetFieldValueDate('D111', sysdate);  
    
    Z_DBF_FILE.OutputRow;  
  end loop;  
  
  :res := Z_DBF_FILE.getBlob;  
end;